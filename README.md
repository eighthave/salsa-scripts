# salsa migration scripts

For the lazy people like me, here are a few simple scripts to interact with
gitlab's API and setup projects on Salsa (salsa.debian.org).

* `import.sh` https://anonscm.debian.org/git/pkg-foo/bar.git pkg-baz

  Creates a new project 'bar' in group 'pkg-baz' from an old git repo hosted
in 'https://anonscm.debian.org/git/pkg-foo/bar.git'

* `irker.sh` foo \#debian-bar

  Sets up IRC notifications via Irker service for project 'foo' on IRC channel '#debian-bar'

* `kgb.sh` pkg-bar/foo debian-bar

  Sets up IRC notifications via KGB for project 'pkg-bar/foo' on IRC channel '#debian-bar'

* `irker_to_kgb.sh` pkg-bar/foo

  (recursively) Migrate a group's projects from irker to kgb

* `hook_tagpending.sh` foo

  Sets up a webhook for project 'foo' which marks bug appearing in commit
messages as 'pending' on Debian BTS.

* `hook_close.sh` foo

  Sets up a webhook for project 'foo' which closes Debian BTS bugs appearing
in commit messages.

* `emails_on_push.sh` foo "email addresses"

  Sets up email notifications on push for project 'foo'. Emails will be sent
  to "email addresses" (this second argument can be omitted, in which case
  emails will be sent to the Debian Package Tracker through
  dispatch@tracker.debian.org).

* `update_default_branch.sh` pkg-foo/bar "branch-name"

  Update the default branch of the "bar" project in the "pkg-foo" group to
  be "branch-name". The branch must be already existing for this to work.
  If the branch name is omitted, it will use "debian/master" as
  default branch.

* `list_projects.sh` pkg-baz

  Lists on stdout all the projects of the group pkg-baz, one project per line.
  The list will contain at least public projects, and also private projects to
  which the token gives access to.

In order to use these scripts, you must create a 'salsarc' file with the
following content:

	SALSA_URL="https://salsa.debian.org/api/v4"
	SALSA_TOKEN="...."

You may create API tokens by visiting your [personal access tokens](https://salsa.debian.org/profile/personal_access_tokens)
page.

Documentation of Salsa service can be found [here](https://wiki.debian.org/Salsa/Doc)
